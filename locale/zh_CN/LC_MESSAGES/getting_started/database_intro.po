msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-01 00:42+0000\n"
"PO-Revision-Date: 2023-05-22 14:02\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/digikam-doc/"
"docs_digikam_org_getting_started___database_intro.pot\n"
"X-Crowdin-File-ID: 41517\n"

#: ../../getting_started/database_intro.rst:1
msgid "How to quickly start digiKam photo management program"
msgstr ""

#: ../../getting_started/database_intro.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, database, intro"
msgstr ""

#: ../../getting_started/database_intro.rst:14
msgid "Database"
msgstr ""

#: ../../getting_started/database_intro.rst:17
msgid "Overview"
msgstr ""

#: ../../getting_started/database_intro.rst:19
msgid ""
"Everyone knows about a database; it is used to store data. As all other "
"photographs management programs, digiKam too uses the database for some "
"obvious reasons like avoiding data duplication, reducing data redundancy, a "
"quick search engine, and greater data integrity. Moreover, the cost of data "
"entry, storage and retrieval are drastically reduced. Additionally, any user "
"can access the data using query language."
msgstr ""

#: ../../getting_started/database_intro.rst:21
msgid ""
"Talking in particular about digiKam, the Albums, Album Roots, Tags, "
"Thumbnails, Face Recognition Data, Image Metadata, File Paths, Settings etc. "
"are all stored in different database files."
msgstr ""

#: ../../getting_started/database_intro.rst:23
msgid ""
"The digiKam actually manages more than one database. For convenience, it is "
"broadly categorized in three:"
msgstr ""

#: ../../getting_started/database_intro.rst:25
msgid ""
"Core database for all collection properties, i.e. it hosts all albums, "
"images and searches data."
msgstr ""

#: ../../getting_started/database_intro.rst:27
msgid ""
"Thumbnails database for compressed thumbnails i.e. to host image thumbs "
"using wavelets compression images (**PGF** format)."
msgstr ""

#: ../../getting_started/database_intro.rst:29
msgid ""
"Similarity database to store image finger-prints for fuzzy search engine."
msgstr ""

#: ../../getting_started/database_intro.rst:31
msgid ""
"Face database for storing face recognition metadata i.e. to host face "
"histograms for faces recognition."
msgstr ""

#: ../../getting_started/database_intro.rst:37
msgid ""
"Example of digiKam Remote MySQL Configuration Hosted on a NAS From The Local "
"Network"
msgstr ""

#: ../../getting_started/database_intro.rst:39
msgid ""
"The whole details of database settings are mostly given in the :ref:"
"`Database Setup section <database_settings>`."
msgstr ""

#: ../../getting_started/database_intro.rst:42
msgid "Migrating From Other Software"
msgstr ""

#: ../../getting_started/database_intro.rst:44
msgid ""
"To populate the digiKam database from file properties managed by another "
"software, it's recommend to write all metadata in XMP sidecar files for the "
"best interoperability. digiKam cannot parse the proprietary and closed "
"source database. XMP sidecar is standardized and well documented."
msgstr ""

#: ../../getting_started/database_intro.rst:46
msgid ""
"In digiKam, check well the :ref:`Metadata Setup section <metadata_settings>` "
"to use XMP sidecar. The Advanced panel offers a profiles management to "
"handle special cases while importing and exporting metadata with 3rd-party "
"software."
msgstr ""

#: ../../getting_started/database_intro.rst:48
msgid ""
"Typically, from a fresh installation of digiKam, you can create a new root "
"collection based on your path where images managed by your extra software. "
"The contents will be parsed and the database will be populated with the "
"information found in the XMP sidecar."
msgstr ""

#: ../../getting_started/database_intro.rst:50
msgid ""
"When scan of items is done (this can take a while), you must to see all "
"tags, labels, comments in digiKam items properties."
msgstr ""
