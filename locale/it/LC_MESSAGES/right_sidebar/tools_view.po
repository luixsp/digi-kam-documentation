# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Valter Mura <valtermura@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-03-05 22:21+0100\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../right_sidebar/tools_view.rst:1
msgid "digiKam Right Sidebar Tools View"
msgstr "Vista degli strumenti della barra laterale destra di digiKam"

#: ../../right_sidebar/tools_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, tools, post-processing, export"
msgstr ""
"digiKam, documentazione, manuale utente, gestione fotografie, open source, "
"libero, apprendimento, facile, strumenti, post-elaborazione, esportazione"

#: ../../right_sidebar/tools_view.rst:14
msgid "Tools View"
msgstr "Vista Strumenti"

#: ../../right_sidebar/tools_view.rst:16
msgid "Contents"
msgstr "Contenuto"

#: ../../right_sidebar/tools_view.rst:18
msgid ""
"The tools view from right sidebar hosts the list of actions available to "
"process actions on selection. The list is an icon-view sorted by categories "
"of tools. This view give a quick preview of all actions from main menu. For "
"example, with the Album View, you can process in batch selected items with "
"an OCR engine to recognize characters from scanned text."
msgstr ""
"La vista degli strumenti dalla barra laterale destra ospita l'elenco di "
"azioni disponibili da eseguire sulla selezione. L'elenco è una vista a icone "
"ordinata per categorie di strumenti. Questa vista fornisce un'anteprima "
"rapida di tutte le azioni dal menu principale. Per esempio, nella vista "
"Album puoi elaborare in serie gli elementi selezionati con un motore OCR in "
"modo da riconoscere i caratteri dal testo analizzato."

#: ../../right_sidebar/tools_view.rst:24
msgid "The Tools View From Right Sidebar"
msgstr "La vista Strumenti dalla barra laterale destra"
