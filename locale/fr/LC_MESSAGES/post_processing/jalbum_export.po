msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-02-16 16:14+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../post_processing/jalbum_export.rst:1
msgid "The digiKam JAlbum Export"
msgstr ""

#: ../../post_processing/jalbum_export.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, jalbum, gallery, export"
msgstr ""

#: ../../post_processing/jalbum_export.rst:14
msgid "JAlbum Export"
msgstr ""

#: ../../post_processing/jalbum_export.rst:16
msgid ""
"The JAlbum Export tool allows to create metadata for a set on digiKam items "
"to use in the Java-based `JAlbum HTML Gallery Generator <https://en."
"wikipedia.org/wiki/JAlbum>`_. The `Java virtual machine <https://en."
"wikipedia.org/wiki/Java_virtual_machine>`_ and the JAlbum program must be "
"installed previously on your computer as run-time dependencies of this tool."
msgstr ""

#: ../../post_processing/jalbum_export.rst:18
msgid ""
"To run this tool, choose :menuselection:`Tools --> Create JAlbum Gallery` "
"menu entry, or click on **Create JAlbum Gallery** icon from **Tools** tab in "
"**Right Sidebar**."
msgstr ""

#: ../../post_processing/jalbum_export.rst:24
msgid "The Selection of Item to export to JAlbum Gallery Generator"
msgstr ""

#: ../../post_processing/jalbum_export.rst:26
msgid ""
"This opens the JAlbum Export wizard that guides you through the entire "
"process. This first page allows to use the items selection method: from "
"**Images** currently selected in digiKam, or from **Albums** hosted in your "
"collections. This page also verifies the availability of **JAlbum** and "
"**Java** binary programs."
msgstr ""

#: ../../post_processing/jalbum_export.rst:32
msgid "The JAlbum Export Wizard Welcome Page"
msgstr ""

#: ../../post_processing/jalbum_export.rst:34
msgid ""
"The second page of this wizard allows to review the list of images, or "
"albums contents to export in the JAlbum HTML gallery generator. When "
"selection is done, press the **Next** button."
msgstr ""

#: ../../post_processing/jalbum_export.rst:40
msgid "The JAlbum Export Wizard Items Selection Page"
msgstr ""

#: ../../post_processing/jalbum_export.rst:42
msgid ""
"The third page will configure the JAlbum settings to export contents. Here "
"you can configure the **Project Title** and the **Projects folder** where "
"files must be generated."
msgstr ""

#: ../../post_processing/jalbum_export.rst:48
msgid "The JAlbum Export Wizard Settings Page"
msgstr ""

#: ../../post_processing/jalbum_export.rst:50
msgid ""
"The fourth page will show all stages to generate files to export to JAlbum. "
"When all is completed, the JAlbum"
msgstr ""

#: ../../post_processing/jalbum_export.rst:56
msgid "The JAlbum Export Wizard Preparing Output Files"
msgstr ""

#: ../../post_processing/jalbum_export.rst:58
msgid ""
"When all tasks are completed, the JAlbum Application is started with all "
"files exported from digiKam. At this time, you can close the JAlbum Export "
"tool and continue to work with JAlbum application."
msgstr ""

#: ../../post_processing/jalbum_export.rst:64
msgid "The JAlbum Application Started with the Exported Files from digiKam"
msgstr ""
