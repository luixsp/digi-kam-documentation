# translation of docs_digikam_org_maintenance_tools___maintenance_metadata.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_digikam_org_maintenance_tools___maintenance_metadata\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-03-19 10:21+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../maintenance_tools/maintenance_metadata.rst:1
msgid "digiKam Maintenance Tool to Synchronize Metadata"
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, maintenance, metadata, synchronizer"
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:14
msgid "Metadata Synchronizer"
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../maintenance_tools/maintenance_metadata.rst:22
msgid "The digiKam Maintenance Options to Synchronize Metadata"
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:24
msgid ""
"This process synchronize items metadata with database contents. The "
"operation **Direction** can be:"
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:26
msgid "From the database to files."
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:28
msgid "From files to the database."
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:32
msgid ""
"As synchronization is a time consuming process, especially when metadata are "
"written in files, it's a good idea to restrict the job to certain albums or "
"tags."
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:34
msgid ""
"The synchronization depends of the settings from :menuselection:`Settings --"
"> Configure digiKam...` and **Metadata** page. All these settings is "
"described in :ref:`the dedicated section <metadata_settings>` from **Setup "
"Application**."
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:36
msgid ""
"While the metadata synchronizer process is under progress, as the process "
"may take much time and digiKam cannot be used, a non modal dialog appear to "
"make sure that no database corruption occurs."
msgstr ""

#: ../../maintenance_tools/maintenance_metadata.rst:42
msgid "The Metadata Synchronization Process Running in Background"
msgstr ""
