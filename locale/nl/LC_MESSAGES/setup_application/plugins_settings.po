# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-02-27 14:29+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../setup_application/plugins_settings.rst:1
msgid "digiKam Plugins Settings"
msgstr "Instellingen van plug-ins van digiKam"

#: ../../setup_application/plugins_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, plugins, generic, editor, batch queue, loaders"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, plug-ins, generiek, bewerker, takenwachtrij, laders"

#: ../../setup_application/plugins_settings.rst:14
msgid "Plugins Settings"
msgstr "Instellingen van plug-ins van digiKam"

#: ../../setup_application/plugins_settings.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../setup_application/plugins_settings.rst:18
msgid ""
"This view allows to see the list of plugins available for different part of "
"the program:"
msgstr ""
"Deze weergave biedt het bekijken van de lijst met beschikbare plug-ins voor "
"verschillende delen van het programma:"

#: ../../setup_application/plugins_settings.rst:20
msgid ""
"Generic: tools shared between Album View, Light Table and Image Editor. "
"These tools are also available in Showfoto."
msgstr ""
"Algemeen: hulpmiddelen gedeeld tussen Albumweergave, Lichttafel en "
"Afbeeldingsbewerker. Deze hulpmiddelen zijn ook beschikbaar in Showfoto."

#: ../../setup_application/plugins_settings.rst:26
msgid "The Generic Plugins Setup Page"
msgstr "De algemene instellingenpagina voor plug-ins"

#: ../../setup_application/plugins_settings.rst:28
msgid ""
"Image Editor: specific tools to edit item in editor. These tools are also "
"available in Showfoto."
msgstr ""
"Afbeeldingsbewerker: specifieke hulpmiddelen om item te bewerken in de "
"bewerker. Deze hulpmiddelen zijn ook beschikbaar in Showfoto."

#: ../../setup_application/plugins_settings.rst:34
msgid "The Image Editor Plugins Setup Page"
msgstr "De instellingenpagina voor plug-ins voor de afbeeldingsbewerker"

#: ../../setup_application/plugins_settings.rst:36
msgid "Batch Queue Manager: specific digiKam tools to process item in batch."
msgstr ""
"Takenwachtrijbeheerder: specifieke hulpmiddelen van digiKam om items in bulk "
"te verwerken."

#: ../../setup_application/plugins_settings.rst:42
msgid "The Batch Queue Manager Plugins Setup Page"
msgstr "De instellingenpagina voor plug-ins van de takenwachtrijbeheerder"

#: ../../setup_application/plugins_settings.rst:44
msgid ""
"Image Loaders: tools to load item contents in memory. These tools are also "
"available in Showfoto."
msgstr ""
"Afbeeldingsladers: hulpmiddelen om inhoud van een item te laden in geheugen. "
"Deze hulpmiddelen zijn ook beschikbaar in Showfoto."

#: ../../setup_application/plugins_settings.rst:50
msgid "The Image Loaders Plugins Setup Page"
msgstr "De instellingenpagina voor plug-ins voor de afbeeldingsladers"

#: ../../setup_application/plugins_settings.rst:52
msgid ""
"If you double-click on one plugin entry in a list, you will open a dialog "
"showing details about the tool."
msgstr ""
"Als u dubbelklikt op één plug-in in een lijst, opent u een dialoog die "
"details toont over het hulpmiddel."
