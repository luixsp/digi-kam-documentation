# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-01-14 22:31+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../image_editor/effects_tools.rst:1
msgid "digiKam Image Editor Effects Tools"
msgstr "Інструменти ефектів редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, image, editor, blur, charcoal, solarize, vivid, neon, edges, "
"lut3D, distortion, emboss, film, grain, oil, paint, rain, drops"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, зображення, редактор, розмивання, "
"розмиття, вугілля, вигорання, яскравий, неон, краї, lut3D, викривлення, "
"барельєф, плівка, зерно, зернистість, олія, фарба, дощ, краплі"

#: ../../image_editor/effects_tools.rst:14
msgid "Effects Tools"
msgstr "Інструменти ефектів"

#: ../../image_editor/effects_tools.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../image_editor/effects_tools.rst:21
msgid "Blur FX"
msgstr "Додаткові ефекти розмивання"

#: ../../image_editor/effects_tools.rst:23
msgid ""
"The digiKam Blur FX is a series of blurring effects for digital still images."
msgstr ""
"Набір ефектів «Ефекти розмивання» є набором ефектів розмивання для цифрових "
"фотографій."

#: ../../image_editor/effects_tools.rst:25
msgid ""
"With this filter set, you can transform an ordinary photograph into a work "
"of art suitable for framing using blurring operations."
msgstr ""
"За допомогою цього набору фільтрів ви зможете за допомогою дій з розмивання "
"перетворити звичайну фотографію на твір мистецтва, гідний створення картинки "
"у рамці."

#: ../../image_editor/effects_tools.rst:31
msgid "The digiKam Image Editor Blur FX Tool"
msgstr "Інструмент ефектів розмивання редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:33
msgid "These are the blurring effects available:"
msgstr "Ось перелік можливих ефектів розмивання:"

#: ../../image_editor/effects_tools.rst:35
msgid ""
"**Zoom Blur**: blurs the image along radial lines starting from a specified "
"center point. This simulates the blur of a zooming camera, thereby giving "
"the photograph a dynamic expression as often seen in sport photography."
msgstr ""
"**Розмивання трансфокацією**: розмивання зображення відбувається вздовж "
"радіальних ліній, що виходять з вказаної центральної точки. Цей ефект імітує "
"розмивання внаслідок зміни фокусування об’єктива, надаючи фотографії "
"динамічності, яку часто можна помітити у спортивній фотографії."

#: ../../image_editor/effects_tools.rst:37
msgid ""
"**Radial Blur**: blurs the image by rotating the pixels around the specified "
"center point. This simulates the blur of a rotating camera."
msgstr ""
"**Радіальне розмивання**: розмивання зображення відбувається внаслідок "
"обертання пікселів навколо вказаної центральної точки. Цей ефект імітує "
"розмивання внаслідок обертання фотоапарата."

#: ../../image_editor/effects_tools.rst:39
msgid ""
"**Far Blur**: blurs the image to simulate the effect of an unfocused camera "
"lens. The subject seems to recede into the background."
msgstr ""
"**Розмивання віддаленням**: розмивання зображення імітує ефект "
"розфокусування об’єктива фотоапарата. Об’єкт фотографування неначебто "
"пересувається у тло."

#: ../../image_editor/effects_tools.rst:41
msgid ""
"**Motion Blur**: blurs the image by swishing the pixels horizontally. This "
"simulates the blur of a linearly moving camera, i.e. like a shot taken from "
"a car or train."
msgstr ""
"**Розмивання рухом**: розмивання відбувається пересуванням пікселів у "
"горизонтальному напрямку. Цей ефект імітує розмивання внаслідок лінійного "
"пересування фотоапарата, тобто знімки, виконані з автомобіля або потяга, що "
"рухаються."

#: ../../image_editor/effects_tools.rst:43
msgid ""
"**Focus Blur**: blurs the image corners to reproduce the astigmatism "
"distortion of a lens."
msgstr ""
"**Фокусне розмивання**: розмивання у кутах зображення, що відтворює "
"астигматичне викривлення внаслідок недосконалості об’єктива."

#: ../../image_editor/effects_tools.rst:45
msgid ""
"**Softener Blur**: blurs the image softly in the darker tones and strongly "
"in the high lights. This gives photographs a dreamy and glossy soft focus "
"effect (Hamilton effect). It's ideal for creating romantic portraits, "
"glamour photography, or adding a warm and subtle glow."
msgstr ""
"**Розмивання пом’якшенням**: трошки розмиває зображення у областях з темними "
"тонами і сильно розмиває світлі ділянки. Цей фільтр надає фотографіям "
"замріяного і глянсуватого м’якого ефекту фокусування (ефекту Гамільтона). "
"Цей ефект ідеальний для створення романтичних портретів, гламурних "
"фотографій або додавання до фотографії тепла і відтінку сяйва."

#: ../../image_editor/effects_tools.rst:47
msgid ""
"**Shake Blur**: blurs the image by randomly moving the pixels simulating the "
"blur of an arbitrarily moving camera."
msgstr ""
"**Розмивання перескакуванням**: розмиває зображення випадковим пересуванням "
"пікселів, імітуючи розмивання випадковими рухами фотоапарата."

#: ../../image_editor/effects_tools.rst:49
msgid ""
"**Smart Blur**: finds the edges of color in photograph and blurs them "
"without muddying the rest of the image."
msgstr ""
"**Кмітливе розмивання**: програма знаходить краї кольорових ділянок "
"зображення і розмиває їх без засмічування решти зображення."

#: ../../image_editor/effects_tools.rst:51
msgid ""
"**Frost Glass**: blurs the image by simulating randomly dispersing light "
"filtering through hoarse frosted glass."
msgstr ""
"**Матове скло**: розмиває зображення, імітуючи випадкове розсіювання світла, "
"яке є наслідком споглядання зображення крізь матове прозоре скло."

#: ../../image_editor/effects_tools.rst:53
msgid ""
"**Mosaic**: blurs the image by dividing the photograph into rectangular "
"cells and then recreates it by filling those cells with average pixel value."
msgstr ""
"**Мозаїка**: розмиває зображення поділом фотографії на прямокутні комірки з "
"наступним повторним складанням зображення з комірок, заповнених середнім "
"кольором пікселів комірки."

#: ../../image_editor/effects_tools.rst:57
msgid ""
"Some effects can take a long time to run and cause high CPU load. You can "
"always abort an effect by pressing the **Abort** button during preview "
"rendering."
msgstr ""
"Застосування деяких ефектів може тривати досить довго через значне "
"навантаження на процесор комп’ютера. Ви можете скасувати застосування ефекту "
"натисканням кнопки **Перервати**."

#: ../../image_editor/effects_tools.rst:62
msgid "Charcoal Drawing"
msgstr "Малювання вугільним олівцем"

#: ../../image_editor/effects_tools.rst:64
msgid ""
"The digiKam Charcoal is an effect filter that creates a charcoal sketch-like "
"result."
msgstr ""
"За допомогою фільтра digiKam «Малювання крейдою» можна створити ефект ескіза "
"крейдою."

#: ../../image_editor/effects_tools.rst:66
msgid ""
"The digiKam Charcoal filter uses the gradients of color and luminosity to "
"produce a grey scale charcoal sketch. The lines defining the outline of the "
"image are pronounced. Images with slowly changing gradients are not ideal "
"for this effect. It is helpful to imagine what scene you would pick to do as "
"a hand sketch yourself, in order to choose the image to start with."
msgstr ""
"У фільтрі «Малювання вугільним олівцем» digiKam градієнти кольору і "
"освітленості буде використано для створення ескіза «вугільним олівцем» у "
"тонах сірого. За допомогою ліній буде створено контури об’єктів зображення. "
"Якщо на зображенні немає різких змін градієнтів, таке зображення не є "
"ідеальним для застосування цього ефекту. Корисно уявити собі сцену на ескізі "
"створеному вручну, щоб правильно вибрати зображення, до якого б пасував цей "
"ефект."

#: ../../image_editor/effects_tools.rst:72
msgid "The digiKam Image Editor Charcoal Tool"
msgstr "Інструмент малювання вугільним олівцем редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:74
msgid ""
"There are two sliders to control the effect on a scale of 1-100. The upper "
"slider selects the **Pencil size**, whereas the second slider adjusts the "
"contrast (**Smooth**)."
msgstr ""
"Передбачено два повзунки для керування масштабом ефекту 1-100. За допомогою "
"верхнього повзунка можна вибрати розмір пензля, а другий повзунок налаштовує "
"контрастність (**Розмивання**)."

#: ../../image_editor/effects_tools.rst:78
msgid "The result can be improved by adjusting the luminosity levels."
msgstr "Результат можна покращити зміною рівнів освітленості."

#: ../../image_editor/effects_tools.rst:83
msgid "Color Filter Effects"
msgstr "Ефекти фільтрування кольорів"

#: ../../image_editor/effects_tools.rst:85
msgid ""
"The digiKam Color Effects tool provides four color effects: a Solarization "
"effect, a Velvia filter, Neon effect, Edge filter, and Lut3D."
msgstr ""
"У інструменті digiKam «Ефекти кольорів» передбачено чотири ефекти кольорів: "
"ефект вигорання, ефект вельвії, ефект неонового світла, ефект краю та ефект "
"Lut3D."

#: ../../image_editor/effects_tools.rst:87
msgid ""
"In the age of chemical image processing, solarizing (also known as Sabatier) "
"was an effect created by exposing a partially developed print to a brief "
"flash of light, then completing the development. The colored, darker areas "
"shield the additional light from the sensitive photo layers, which has the "
"net effect of making the lighter areas darker and colors being inverted "
"during the second exposure. The result resembles a partially negative image. "
"The tool allows to adjust the interesting effect smoothly."
msgstr ""
"У добу створення знімків за допомогою хімічного процесу, ефект вигорання "
"(також відомий як ефект Сабатьє) створювали експонуванням частково "
"проявленого знімка за допомогою короткого спалаху світла з наступним "
"завершення процедури проявлення. Зафарбовані, темніші ділянки знімка "
"захищали чутливі шари фотопаперу від потрапляння додаткового світла, що "
"відповідно робило світліші ділянки темнішими та призводило до інверсії "
"кольорів під час другого експонування. Результат частково нагадував негатив. "
"За допомогою відповідного інструменту програми ви зможете з легкістю "
"налаштувати параметри цього цікавого ефекту."

#: ../../image_editor/effects_tools.rst:92
msgid "The Solarization Effect"
msgstr "Ефект вигорання"

#: ../../image_editor/effects_tools.rst:94
msgid ""
"The **Level** control helps to preview the solarization by simply increasing "
"it. At about 50% intensity the image shows what was once chemically "
"possible. If you further increase the effect it will finally become a "
"negative image, a stage of inversion not achievable on photographic paper."
msgstr ""
"За допомогою пункту **Рівень** ви зможете спостерігати за вигоранням, "
"збільшуючи значення параметра. На рівні інтенсивності, близькому до 50%, "
"буде показано зображення з найбільшим рівнем вигорання, якого можна досягти "
"під час хімічної обробки. Якщо збільшувати інтенсивність, зрештою ви "
"отримаєте зображення негатива, тобто досягнете етапу інвертування, якого не "
"можна досягти за допомогою фотографічного паперу."

#: ../../image_editor/effects_tools.rst:100
msgid "The digiKam Image Editor Solarize Tool"
msgstr "Інструмент вигорання редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:105
msgid "The Vivid Effect"
msgstr "Ефект чіткості"

#: ../../image_editor/effects_tools.rst:107
msgid ""
"The vivid filter simulates what is known as *Velvia* effect. It is different "
"from saturation in that it has a more pronounced contrast effect that bring "
"colors brilliantly alive and glowing. Try it, it renders beautiful for many "
"subjects."
msgstr ""
"Фільтр чіткості імітує те, що відоме під назвою ефекту «вельвії». Наслідки "
"застосування цього ефекту відрізняються від наслідків зміни насиченості: за "
"допомогою цього ефекту можна отримати зображення з вираженішою "
"контрастністю, кольори якого будуть яскраво-живими і сяючими. Спробуйте "
"його, він може надати нових фарб багатьом зображенням!"

#: ../../image_editor/effects_tools.rst:109
msgid ""
"Velvia is a brand of daylight-balanced color reversal film produced by the "
"Japanese company Fujifilm. The name is a contraction of *Velvet Media*, a "
"reference to its smooth image structure. Velvia has very saturated colors "
"under daylight, high contrast. These characteristics make it the slide film "
"of choice for most nature photographers. Velvia's highly saturated colors "
"are, however, considered overdone by some photographers, especially those "
"who don't primarily shoot landscapes. You can control the Velvia intensity "
"with the **level** setting."
msgstr ""
"Вельвія (Velvia) — це марка збалансованої для денного світла реверсивної "
"плівки, яку виробляла японська компанія Fujifilm. Назва є скороченням "
"комбінації слів «Velvet Media» (оксамитовий носій), цю назву пов’язано з "
"гладкістю структури зображень, які можна було створити за допомогою цієї "
"плівки. Вельвія давала дуже насичені кольори з високою контрастністю у "
"денному світлі. Таке поєднання характеристик зробило цю плівку вибором "
"більшості фотографів-пейзажистів. Але дуже насичені кольори вельвії "
"вважалися підробними багатьма фотографами, особливо тими, для яких зйомки "
"пейзажів не були основним творчим уподобанням. Керувати інтенсивністю "
"вельвії можна за допомогою параметра **Рівень**."

#: ../../image_editor/effects_tools.rst:115
msgid "The digiKam Image Editor Vivid Tool"
msgstr "Інструмент яскравих кольорів редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:120
msgid "The Neon Effect"
msgstr "Ефект неонового світла"

#: ../../image_editor/effects_tools.rst:122
msgid ""
"The neon filter simulates neon light along the contrast edges. The **Level** "
"parameter controls the lightness of the result, whereas the **Iteraction** "
"slider determines the thickness of the neon light. With big images, the "
"filter might eat the CPU time for a moment."
msgstr ""
"Неоновий фільтр імітує ефекти неонового світла навколо контрастних країв "
"елементів зображення. За допомогою параметра рівня можна керування "
"освітленістю результату, а повзунок взаємодії визначає ширину області "
"неонового світла. Якщо розміри зображення досить великі, застосування цього "
"фільтра може на деякий час значно навантажити процесор комп’ютера."

#: ../../image_editor/effects_tools.rst:128
msgid "The digiKam Image Editor Neon Tool"
msgstr "Інструменти неонового світла редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:133
msgid "The Find Edges Effect"
msgstr "Ефект пошуку країв"

#: ../../image_editor/effects_tools.rst:135
msgid ""
"The find edges filter detects the edges in a photograph and their strength. "
"With the **Level** parameter, a low value results in black, high-contrasted "
"image with thin edges. A high value results in thick edges with low contrast "
"and many colors in dark areas. **Iteration** allows to increase or decrease "
"the frame presence in image."
msgstr ""
"Фільтр пошуку країв виявляє краї ділянок на фотографії і визначає їхню "
"різкість. Для параметра **Рівень** нижнє значення дає чорне, "
"висококонтрастне зображення з тонкими краями. Високе значення дає товсті "
"краї із низькою контрастністю та широким діапазоном кольорів на темних "
"ділянках. За допомогою параметра **Ітерація** можна збільшити або зменшити "
"наявність рамок на зображенні."

#: ../../image_editor/effects_tools.rst:141
msgid "The digiKam Image Editor Find Edges Tool"
msgstr "Інструмент пошуку країв редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:146
msgid "The Lut3D Effect"
msgstr "Ефект Lut3D"

#: ../../image_editor/effects_tools.rst:148
msgid ""
"The Lut3D filter coloring images by applying transformation based on a 3D "
"lookup table."
msgstr ""
"Фільтр Lut3D розфарбовує зображення застосуванням перетворення на основі "
"тривимірної таблиці пошуку."

#: ../../image_editor/effects_tools.rst:150
msgid ""
"In the film industry, 3D lookup tables are used to map one color space to "
"another. They are commonly used to calculate preview colors for a monitor or "
"digital projector of how an image will be reproduced on another display "
"device, typically the final digitally projected image or release print of a "
"movie. A 3D LUT is a 3D lattice of output RGB color values that can be "
"indexed by sets of input RGB colour values. Each axis of the lattice "
"represents one of the three input color components and the input color thus "
"defines a point inside the lattice. Since the point may not be on a lattice "
"point, the lattice values must be interpolated; most products use trilinear "
"interpolation."
msgstr ""
"У відеоіндустрії просторові таблиці пошуку, які використовують для "
"прив'язування одного простору кольорів до іншого. Такі таблиці типово "
"використовують для обчислення кольорів попереднього перегляду для монітора "
"або цифрового проєктора. Це впливає на те, як зображення буде показано на "
"іншому пристрої для показу, типово, остаточній цифровій проєкції зображення "
"або остаточній версії відео. Просторова таблиця пошуку є просторовою ґраткою "
"значень кольорів RGB результату, яку можна індексувати за наборами вхідних "
"значень кольорів RGB. Кожна вісь ґратки відповідає за один з трьох "
"компонентів вхідного кольору, а отже, вхідний колір визначає точку у ґратці. "
"Оскільки точка може лежати поза основною ґраткою, значення точок ґратки "
"використовують для інтерполяції. У більшості продуктів використовують "
"трилінійну інтерполяцію."

#: ../../image_editor/effects_tools.rst:152
msgid ""
"The tool provide a list of Lut3D to apply on the image, including preview "
"thumbnail of the effect. **Intensity** setting allows to adjust the colors "
"overall of the effect on image."
msgstr ""
"Інструмент надає доступ до списку просторових таблиць пошуку (Lut3D), які "
"слід застосувати до зображення, а також мініатюр попереднього перегляду "
"застосування ефекту. За допомогою параметра **Інтенсивність** можна "
"скоригувати загальні кольори ефекту на зображенні."

#: ../../image_editor/effects_tools.rst:158
msgid "The digiKam Image Editor Lut3D Tool"
msgstr "Інструменти Lut3D редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:163
msgid "Distortion FX"
msgstr "Ефект викривлення"

#: ../../image_editor/effects_tools.rst:165
msgid ""
"The digiKam Distortion FX is a series of distorting effects for digital "
"still images."
msgstr ""
"Набір ефектів «Ефекти викривлення» digiKam є набором ефектів викривлення для "
"цифрових фотографій."

#: ../../image_editor/effects_tools.rst:167
msgid ""
"With this filter set, you can transform an ordinary photograph into a work "
"of art suitable for framing using distorting operations."
msgstr ""
"За допомогою цього набору фільтрів ви зможете за допомогою дій з викривлення "
"перетворити звичайну фотографію на твір мистецтва, гідний створення картинки "
"у рамці."

#: ../../image_editor/effects_tools.rst:173
msgid "The digiKam Image Editor Distortion FX Tool"
msgstr "Інструмент ефектів викривлення редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:175
msgid "These are the distorting effects available:"
msgstr "Ось перелік можливих ефектів викривлення:"

#: ../../image_editor/effects_tools.rst:177
msgid ""
"**Fish Eyes**: warps the photograph around a 3D spherical shape to reproduce "
"the common photograph 'Fish Eyes' effect."
msgstr ""
"«Риб’ячі очі»: натягує фотографію на просторову сферичну поверхню для "
"відтворення типового фотографічного ефекту «риб’ячого ока»."

#: ../../image_editor/effects_tools.rst:179
msgid "**Twirl**: spins the photograph to produce a Twirl pattern."
msgstr "«Вихор»: закручує фотографію, що створює ефекти вихору."

#: ../../image_editor/effects_tools.rst:181
msgid ""
"**Cylinder Horizontal**: warps the photograph around a horizontal cylinder."
msgstr "**Циліндричне гор.**: натягує фотографію на горизонтальний циліндр."

#: ../../image_editor/effects_tools.rst:183
msgid "**Cylinder Vertical**: warps the photograph around a vertical cylinder."
msgstr "**Циліндричне верт.**: натягує фотографію на вертикальний циліндр."

#: ../../image_editor/effects_tools.rst:185
msgid ""
"**Cylinder H/V**: warps the photograph around a 2 cylinders, vertical and "
"horizontal."
msgstr ""
"**Циліндричне Г/В**: натягує фотографію одразу на 2 циліндри, вертикальний і "
"горизонтальний."

#: ../../image_editor/effects_tools.rst:187
msgid "**Caricature**: distorts photograph with 'Fish Eyes' effect inverted."
msgstr ""
"**Карикатура**: викривляє зображення за допомогою ефекту, зворотного до "
"ефекту «Риб’ячі очі»."

#: ../../image_editor/effects_tools.rst:189
msgid ""
"**Multiple Corners**: splits the photograph like a multiple corners pattern."
msgstr ""
"**Декілька кутів**: розбиває фотографію на візерунок з декількома кутами."

#: ../../image_editor/effects_tools.rst:191
msgid "**Waves Horizontal**: distorts the photograph with horizontal waves."
msgstr "**Хвилі гор.**: викривляє зображення у формі горизонтальних хвиль."

#: ../../image_editor/effects_tools.rst:193
msgid "**Waves Vertical**: distorts the photograph with vertical waves."
msgstr "**Хвилі верт.**: викривляє зображення у формі вертикальних хвиль."

#: ../../image_editor/effects_tools.rst:195
msgid ""
"**Block Waves 1**: divides the image into cells and makes it look as if it "
"is being viewed through glass blocks."
msgstr ""
"**Блокові хвилі 1**: поділяє зображення на комірки, а потім створює ефект "
"перегляду зображення крізь стіну зі скляних блоків."

#: ../../image_editor/effects_tools.rst:197
msgid ""
"**Block Waves 2**: like Block Waves 1 but with another version of glass "
"blocks distortion."
msgstr ""
"**Блокові хвилі 2**: подібний до Блокових хвиль 1, але з використанням "
"іншого різновиду викривлення скляними блоками."

#: ../../image_editor/effects_tools.rst:199
msgid "**Circular Waves 1**: distorts the photograph with circular waves."
msgstr "**Кругові хвилі 1**: викривляє зображення у формі кругових хвиль."

#: ../../image_editor/effects_tools.rst:201
msgid "**Circular Waves 2**: other variation of Circular Waves effect."
msgstr "**Кругові хвилі 2**: інший варіант ефекту кругових хвиль."

#: ../../image_editor/effects_tools.rst:203
msgid ""
"**Polar Coordinates**: converts the photograph from rectangular to polar "
"coordinates."
msgstr ""
"**Поляризація**: перетворює прямокутні декартові координати фотографії на "
"полярні."

#: ../../image_editor/effects_tools.rst:205
msgid "**Unpolar Coordinates**: Polar Coordinate effect inverted."
msgstr ""
"**Розполяризація координат**: ефект, протилежний до ефекту «Поляризація»."

#: ../../image_editor/effects_tools.rst:207
msgid ""
"**Tiles**: splits the photograph into square blocks and move them randomly "
"inside the image."
msgstr ""
"**Мозаїка**: фотографію буде поділено на квадратні блоки, які потім буде "
"випадковим чином пересунуто зображенням."

#: ../../image_editor/effects_tools.rst:211
msgid ""
"Some effects can take a long time to run and cause high CPU load. You can "
"always abort an effect by pressing **Abort** button during preview rendering."
msgstr ""
"Застосування деяких ефектів може тривати досить довго через значне "
"навантаження на процесор комп’ютера. Ви можете скасувати застосування ефекту "
"натисканням кнопки **Перервати**."

#: ../../image_editor/effects_tools.rst:216
msgid "Emboss Image"
msgstr "Ефект барельєфа для зображення"

#: ../../image_editor/effects_tools.rst:218
msgid ""
"The digiKam Emboss filter sculptures your image into 3-D as if it were "
"stamped into wax."
msgstr ""
"Фільтр «Барельєф» digiKam створює просторове зображення вашої фотографії "
"так, неначебто її було відтиснуто у воску."

#: ../../image_editor/effects_tools.rst:220
msgid ""
"The digiKam Emboss filter is a quick tool to render your images in a 3-D "
"effect. It works particularly well on images with simple structure where "
"color is not the most important content. The filter uses the difference "
"between colors and luminosity to convert it into a grey, moon-like landscape "
"lit from 10 o'clock."
msgstr ""
"Фільтр «Барельєф» digiKam є швидким інструментом для створення на ваших "
"фотографіях просторового ефекту. Цей фільтр досить добре працює для "
"зображень з простою структурою, на яких колір не є найважливішою складовою "
"зображення. Для роботи фільтр використовує різницю між кольорами і "
"освітленістю для перетворення зображення на сірий, подібний до місячного, "
"пейзаж з джерелом світла, розташованим згори ліворуч."

#: ../../image_editor/effects_tools.rst:222
msgid ""
"The **Depth** control allows to define the contrast of the filtering. A "
"value of 30 (10%) is the standard."
msgstr ""
"За допомогою елемента керування **Глибина** ви можете визначити "
"контрастність фільтрування. Типовим є значення 30 (10%)."

#: ../../image_editor/effects_tools.rst:228
msgid "The digiKam Image Editor Emboss Tool"
msgstr "Інструмент барельєфа редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:233
msgid "Film Grain"
msgstr "Зернистість фотоплівки"

#: ../../image_editor/effects_tools.rst:235
msgid ""
"The digiKam Filmgrain filter reproduces traditional film grain techniques of "
"high speed films."
msgstr ""
"Фільтр digiKam «Зернистість плівки» імітує зображення, створені за допомогою "
"традиційних високошвидкісних зернистих плівок."

#: ../../image_editor/effects_tools.rst:237
msgid ""
"This filter is an easy tool to produce film grain on your images as known "
"from classical high speed film material as, for example, the famous B/W "
"KodaK Tri-X. In order to increase film sensitivity, manufacturers employed "
"larger silver grains in the photo emulsion."
msgstr ""
"Фільтр «зернистість плівки» digiKam — це простий інструмент для відтворення "
"зернистості плівки на ваших фотографіях. Подібну зернистість можна побачити "
"на фотографіях, створених за допомогою класичної високошвидкісної плівки, "
"наприклад, знаменитої плівки B/W KodaK Tri-X. Щоб збільшити чутливість "
"плівки, виробники додавали більші зерна сполук срібла у фотоемульсію."

#: ../../image_editor/effects_tools.rst:239
msgid ""
"The film grain effect gives your shot a particular mood or seems to "
"transport it in time. The treated image acquires a timeless atmosphere, "
"detached from every day life. If you want that gritty, art-house, street-"
"photography grainy film look, especially in monochromatic photos, use this "
"filter."
msgstr ""
"За допомогою ефекту зернистості ви можете надати вашим знімкам певного "
"настрою або неначебто перенести їх у часі. Результатом застосування ефекту "
"буде зображення у дусі непідвладності часу, відокремлене від щоденного "
"життя. Якщо вам потрібна саме ця атмосфера шерехуватості, художності, "
"вуличної фотографії, особливо на чорно-білих зображеннях, скористайтеся цим "
"фільтром."

#: ../../image_editor/effects_tools.rst:245
msgid "The digiKam Image Editor Film Grain Tool"
msgstr "Інструмент зернистості плівки редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:247
msgid ""
"As common settings, a slider allows control the **Grain size** and you can "
"turn on the granularity simulating a photographic distribution. For "
"**Luminance**, **Chrominance Blue**, and **Chrominance Red** channels, you "
"can adjust the grain **Intensity** with a effects on **Shadows**, "
"**Middletones**, and **Highlight**."
msgstr ""
"Як і у загальних параметрах, повзунок надає змогу керувати розміром зерна. "
"Ви можете увімкнути імітації гранул, як на реальних фотознімках. Для каналів "
"освітленості, колірності синього та колірності червоного ви можете "
"скоригувати інтенсивність зерен із впливом на затінені, середні та "
"висвітлені ділянки."

#: ../../image_editor/effects_tools.rst:251
msgid ""
"If you process a black and white image, the grain needs to be applied only "
"on **Luminance** channel."
msgstr ""
"Якщо ви працюєте із чорно-білим зображенням, зернистість має бути "
"застосовано лише до каналу освітленості."

#: ../../image_editor/effects_tools.rst:256
msgid "Oil Paint"
msgstr "Малювання олійними фарбами"

#: ../../image_editor/effects_tools.rst:258
msgid ""
"The digiKam Oil Paint filter gives your image the look of an oilpainting."
msgstr ""
"Фільтр олійного зображення digiKam надає вашому знімку вигляду намальованої "
"олійною фарбою картини."

#: ../../image_editor/effects_tools.rst:260
msgid ""
"This filter gives your digital images a nice oilpainting-like look. Images "
"of nature and still lives are well suited for this effect."
msgstr ""
"За допомогою цього фільтра ви можете надати цифровим фотографіям вигляду "
"картин, намальованих олійною фарбою. Для застосування цього ефекту найкраще "
"пасують зображення природи та натюрморти."

#: ../../image_editor/effects_tools.rst:266
msgid "The digiKam Image Editor Oil Paint Tool"
msgstr "Інструмент малювання олійною фарбою редактора зображень digiKam"

#: ../../image_editor/effects_tools.rst:268
msgid ""
"There are two sliders to control the effect. The upper slider selects the "
"**Brush Size** between 1 and 5. Bigger brushes are better suited for large "
"images. **Smooth** controls the smoothness or, seen from the other end, the "
"jaggedness."
msgstr ""
"Для керування цим ефектом ви можете скористатися двома повзунками. За "
"допомогою верхнього повзунка ви можете вибрати **Розмір пензля** у межах від "
"1 до 5. Більші пензлі пасують до великих зображень. **Розмивання** керує "
"плавністю або, навпаки, зубчастістю мазків."

#: ../../image_editor/effects_tools.rst:273
msgid "Rain Drops"
msgstr "Ефект крапель дощу"

#: ../../image_editor/effects_tools.rst:275
msgid "The digiKam Raindrops filter puts beautiful raindrops on your image."
msgstr ""
"Фільтр digiKam «Краплі дощу» створить на вашому зображенні чудову імітацію "
"крапель дощу."

#: ../../image_editor/effects_tools.rst:277
msgid ""
"The Raindrops is nice little tool to put raindrops onto your images. "
"Naturally, it renders your image in a kind of wet look."
msgstr ""
"«Краплі дощу» — це чудовий невеличкий інструмент, за допомогою якого ви "
"можете додати краплі дощу на ваші зображення. Як зрозуміло з назви, "
"використання цього фільтра надає вашому зображенню вигляду змоченого "
"краплями дощу."

#: ../../image_editor/effects_tools.rst:279
msgid ""
"Three sliders give you control over the effect filter: **Drop size** "
"obviously allows to change the size of the drops. As the drop size doesn't "
"automatically scale with the image size it is often necessary to reduce the "
"size for small images. **Number** changes the number and density of drops. "
"**Fish eye** changes the optical effect of the drops across the image."
msgstr ""
"Керувати ефектом можна за допомогою трьох повзунків. Очевидно, поле **Розмір "
"краплі** надає змогу змінити розмір крапель. Оскільки розмір крапель не "
"змінюється у автоматичному режимі зі зміною розмірів зображення, часто "
"виникає потреба у зменшенні розміру для маленьких зображень. Параметр "
"**Кількість** керує кількістю та щільністю крапель. Параметр **Коефіцієнт "
"«риб’ячого ока»** керує оптичним ефектом у краплях на зображенні."

#: ../../image_editor/effects_tools.rst:283
msgid ""
"You can keep a zone clear of raindrops with the digiKam Image Editor "
"**Select** tool. Selecting the area to avoid (for example a face) before "
"launching the Raindrops filter will keep it free from rain drops."
msgstr ""
"Ви можете створити ділянку, вільну від крапель дощу за допомогою інструменту "
"**Вибір** редактора зображень digiKam. Оберіть ділянку вільну від крапель "
"(наприклад, обличчя) до запуску фільтра «Краплі дощу», щоб наказати програмі "
"звільнити цю ділянку від крапель."

#: ../../image_editor/effects_tools.rst:289
msgid "Rain Drops Tool Applying Effect to Whole Image Excepted on Dog Face"
msgstr ""
"Застосування ефекту крапель дощу до усього зображення, окрім морди собаки"

#~ msgid ""
#~ "digiKam, documentation, user manual, photo management, open source, free, "
#~ "learn, easy"
#~ msgstr ""
#~ "digiKam, документація, підручник користувача, керування фотографій, "
#~ "відкритий код, вільний, навчання, простий"

#~ msgid ""
#~ "The find edges filter detects the edges in a photograph and their "
#~ "strength."
#~ msgstr "Фільтр пошуку країв виявляє краї та їхню потужність на фотографії."

#~ msgid "Three sliders give you control over the effect filter:"
#~ msgstr "Керувати параметрами ефекту ви можете за допомогою трьох повзунків:"
