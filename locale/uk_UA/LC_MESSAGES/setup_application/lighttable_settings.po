# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 00:39+0000\n"
"PO-Revision-Date: 2023-01-05 08:48+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../setup_application/lighttable_settings.rst:1
msgid "digiKam Light Table Settings"
msgstr "Параметри стола з підсвічуванням у digiKam"

#: ../../setup_application/lighttable_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, light, table, configuration"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, стіл, підсвічування, налаштування"

#: ../../setup_application/lighttable_settings.rst:14
msgid "Light Table Settings"
msgstr "Параметри стола з підсвічуванням"

#: ../../setup_application/lighttable_settings.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../setup_application/lighttable_settings.rst:18
msgid ""
"The setting of the digiKam Light Table can be set to default values on this "
"page so that every time you open the Light Table, these settings are "
"activated (if possible, because for images having different sizes the "
"synchronous mode does not work)."
msgstr ""
"Ви можете визначити параметри стола з підсвічуванням digiKam, вказані на цій "
"сторінці, типовими, тобто такими, які буде використано під час кожного "
"завантаження стола з підсвічуванням (якщо це можливо, оскільки для зображень "
"різних розмірів режим синхронізації не працюватиме)."

#: ../../setup_application/lighttable_settings.rst:24
msgid "The digiKam Light Table Configuration Page"
msgstr "Сторінка налаштувань параметрів стола з підсвічуванням у digiKam"

#: ../../setup_application/lighttable_settings.rst:26
msgid ""
"**Synchronize Panels Automatically** refers to the **Synchronize** button on "
"the Light Table toolbar which is to say that you can still switch off this "
"option there. If the next option is not checked you have to load the images "
"from the thumbbar to the two panels of the Light Table by drag+drop or with "
"the **On Left** or **On Right** buttons."
msgstr ""
"Пункт **Синхронізувати панелі автоматично** стосується кнопки "
"**Синхронізувати** панелі інструментів стола з підсвічуванням (там, до речі, "
"ви теж можете вимкнути цей режим). Якщо не позначено наступний пункт, вам "
"доведеться завантажувати зображення із панелі мініатюр на дві інші панелі "
"стола з підсвічуванням перетягуванням зі скиданням або за допомогою кнопок "
"**Ліворуч** і **Праворуч**."
